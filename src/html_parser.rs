use thiserror::Error;
use tl::VDom;

#[derive(Debug, Error)]
pub enum Error {
    #[error("could not parse html: {0}")]
    Parse(String),
}

pub fn parse_html(html: &str) -> Result<VDom, Error> {
    tl::parse(html, tl::ParserOptions::default()).map_err(|e| Error::Parse(e.to_string()))
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod test {
    use super::*;

    static DEFAULT_PAGE_SRC: &str = include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/test_html/2023-12-23/default_page.html"
    ));

    #[test]
    fn test_html() {
        assert!(parse_html(DEFAULT_PAGE_SRC).is_ok());
    }
}
