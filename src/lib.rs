mod html_parser;
mod scrape;

pub mod metadata;
pub mod model;
pub mod timetable;
