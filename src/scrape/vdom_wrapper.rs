use std::rc::Rc;

pub struct Dom<'a> {
    vdom: Rc<tl::VDom<'a>>,
}

impl<'a> Dom<'a> {
    pub fn new(virtual_dom: tl::VDom<'a>) -> Self {
        Dom {
            vdom: Rc::new(virtual_dom),
        }
    }

    pub fn get_element_by_id(&'a self, id: &str) -> Option<Element<'a>> {
        self.vdom.get_element_by_id(id).map(|handle| {
            Element::new(
                self.vdom.clone(),
                // unwrap 1: both the right parser and node handle are enforced
                // upwrap 2: get_element_by_id can only match a tag
                #[allow(clippy::unwrap_used)]
                handle.get(self.vdom.parser()).unwrap().as_tag().unwrap(),
            )
        })
    }
}

#[derive(Debug, Clone)]
pub struct Element<'a> {
    vdom: Rc<tl::VDom<'a>>,
    tag: &'a tl::HTMLTag<'a>,
}

pub enum Attribute {
    None,
    WithoutValue,
    Exists(String),
}

impl<'a> Element<'a> {
    fn new(vdom: Rc<tl::VDom<'a>>, tag: &'a tl::HTMLTag<'a>) -> Self {
        Element { vdom, tag }
    }

    pub fn has_class(&self, class: &str) -> bool {
        match self.tag.attributes().class() {
            Some(bytes) => bytes
                .as_utf8_str()
                .split_ascii_whitespace()
                .any(|pclass| pclass == class),
            None => false,
        }
    }

    pub fn attribute(&self, key: &str) -> Attribute {
        match self.tag.attributes().get(key) {
            None => Attribute::None,
            Some(attribute_option) => match attribute_option {
                None => Attribute::WithoutValue,
                Some(value) => Attribute::Exists(
                    html_escape::decode_html_entities(&value.as_utf8_str()).to_string(),
                ),
            },
        }
    }

    pub fn children(&self) -> Vec<Element> {
        self.tag
            .children()
            .top()
            .iter()
            .filter_map(|&node_handle| self.element_from_node_handle(node_handle))
            .collect::<Vec<Element>>()
    }

    pub fn children_with_class(&self, class: &str) -> Vec<Element> {
        self.tag
            .children()
            .top()
            .iter()
            .filter_map(|&node_handle| self.element_from_node_handle(node_handle))
            .filter(|element| element.has_class(class))
            .collect::<Vec<Element>>()
    }

    pub fn first_child(&self) -> Option<Element> {
        self.tag
            .children()
            .top()
            .iter()
            .find_map(|&node_handle| self.element_from_node_handle(node_handle))
    }

    pub fn first_child_with_class(&self, class: &str) -> Option<Element> {
        self.tag
            .children()
            .top()
            .iter()
            .filter_map(|&node_handle| self.element_from_node_handle(node_handle))
            .find(|element| element.has_class(class))
    }

    pub fn last_child(&self) -> Option<Element> {
        self.tag
            .children()
            .top()
            .iter()
            .filter_map(|&node_handle| self.element_from_node_handle(node_handle))
            .last()
    }

    fn element_from_node_handle(&self, node_handle: tl::NodeHandle) -> Option<Element> {
        // unwrap: both the right parser and the right node handle are enforced
        #[allow(clippy::unwrap_used)]
        node_handle
            .get(self.vdom.parser())
            .unwrap()
            .as_tag()
            .map(|tag| Element::new(self.vdom.clone(), tag))
    }

    pub fn inner_text(&self) -> String {
        html_escape::decode_html_entities(&self.tag.inner_text(self.vdom.parser())).to_string()
    }
}
