use std::collections::HashMap;

use super::{
    vdom_wrapper::{Attribute, Dom},
    Error,
};

pub fn select(dom: &Dom, select_id: &str) -> Result<HashMap<String, String>, Error> {
    let select = dom
        .get_element_by_id(select_id)
        .ok_or(Error::MissingElement(format!("#{select_id}")))?;

    let select_children = select.children();

    Ok(select_children
        .iter()
        .filter_map(|option| {
            // we don't care about valueless attributes
            match option.attribute("value") {
                Attribute::Exists(value) => Some((value, option.inner_text().trim().to_string())),
                _ => None,
            }
        })
        .collect::<HashMap<String, String>>())
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod test {
    use super::*;

    use std::collections::HashMap;

    use test_case::test_case;

    use crate::html_parser;

    macro_rules! map {
        ($($k:expr => $v:expr),* $(,)?) => {{
            core::convert::From::from([$((String::from($k), String::from($v)),)*])
        }};
    }

    static DEFAULT_PAGE_SRC: &str = include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/test_html/2023-12-23/default_page.html"
    ));

    #[test_case("selectedClass", &map! {"2C" => "2.B", "2K" => "2.ME", "2O" => "2.S", "18" => "4.ME", "2E" => "2.E", "33" => "1.V", "2X" => "1.I", "2G" => "2.G", "1R" => "3.J", "1X" => "3.V", "11" => "4.A", "2W" => "1.H", "2T" => "1.D", "2V" => "1.G", "2D" => "2.D", "1L" => "3.D", "1Q" => "3.I", "15" => "4.G", "17" => "4.I", "2Q" => "2.V", "35" => "1.SB", "32" => "1.EM", "2I" => "2.H", "1N" => "3.F", "1S" => "3.ME", "12" => "4.B", "1O" => "3.G", "1K" => "3.B", "16" => "4.H", "1J" => "3.A", "1M" => "3.E", "2S" => "1.B", "30" => "1.PE", "2Z" => "1.ME", "2N" => "2.EM", "1V" => "3.EM", "2U" => "1.E", "14" => "4.E", "2R" => "1.A", "2B" => "2.A", "1P" => "3.H", "34" => "1.SA", "2M" => "2.EL", "2J" => "2.I", "31" => "1.EL", "2L" => "2.PE", "1U" => "3.EL", "13" => "4.D", "2F" => "2.F", "1W" => "3.S"}; "parse class select")]
    #[test_case("selectedTeacher", &map! {"U  67" => "Klečková Zdenka", "UOONR" => "Hůlka Vladimír", "UOONQ" => "Račický Slavomír", "UUORM" => "Raiter Pavel", "UXOS7" => "Budina Martin", "USOR8" => "Retka Tomáš", "UOOOC" => "Hurda Jan", "UZON1" => "Budinová Eva", "UOOP7" => "Macháček Miloslav", "UQOQ4" => "Bajer Libor", "UXOS4" => "Hašková Alena", "UMVMU" => "Jelínek Radek", "UNONK" => "Ludvíková Jiřina", "UXOS6" => "Mannlová Kateřina", "UVORW" => "Novák Pavel", "UWORZ" => "Fereš Pavel", "UOONW" => "Papíková Marie", "UJONA" => "Bubák Petr", "UUORL" => "Jirásek Lubomír", "UTORI" => "Orlíček Miloš", "UUORN" => "Švec Radim", "UYNKH" => "Albrechtová Marie", "UOONU" => "Jiroutová Kateřina", "U7TVP" => "Kvasničková Alena", "UUORP" => "Truncová Tereza", "UPOPI" => "Věcek František", "UY5HV" => "Budina Petr", "UWORX" => "Trubák Zdeněk", "UQOPZ" => "Jelinková Jaroslava", "UOOP9" => "Zahálka Martin", "UMONF" => "Michalec Milan", "U  40" => "Štverák Milan", "U0080" => "Bednaříková Lea", "UOONY" => "Pácalová Pavlína", "UQOPX" => "Hradský Pavel", "UOOO9" => "Huráň Jiří", "U  37" => "Šilar Zdeněk", "UZOMP" => "Štědrý Lukáš", "UVORU" => "Fixa Josef", "UVORS" => "Trojan Zbyněk", "UMONI" => "Koucký Miroslav", "UQOPW" => "Dvořáková Jana", "U  12" => "Hron Vladimír", "UOOP8" => "Kopecký Zbyněk", "U  52" => "Černoch Milan", "UOOO1" => "Kubelka Aleš", "U  10" => "Poláčková Danuše", "UXOS8" => "Psotková Šárka", "UOONP" => "Čáp Pavel", "U   7" => "Forejtová Jaroslava", "UWOS2" => "Tisoň David", "UPOPN" => "Šeda Petr", "UQOPY" => "Jedličková Alena", "UZOM7" => "Jirka Miroslav", "USOR2" => "Kašpar Vladimír", "UEMKE" => "Bartel Jaroslav", "UTMBB" => "Pospíšilová Andrea", "UXOS5" => "Braun Martin", "UE5IF" => "Štěpánek Ladislav", "UE3DL" => "Mikuláš Petr", "UQOQ6" => "Polák Jiří", "UOONZ" => "Kverek Česlav", "UZON6" => "Baklíková Ivona", "USOQW" => "Hrabaň Václav", "UOOO6" => "Hrnčíř Petr", "U0YJ2" => "Jičínský Tomáš", "UOOO0" => "Mejstřík Ivan", "UROQQ" => "Nedbalová Štěpánka", "U0107" => "Reslová Jana", "USORB" => "Cerman Luděk", "UOOO8" => "Rudolf Jiří", "UOONS" => "Hanáková Vladimíra", "UBCDT" => "Sobolová Zdeňka", "UPOPC" => "Bárta Čestmír", "UWOS1" => "Kupka Libor", "USORD" => "Štech Jan", "UUORO" => "Boušková Pavlína", "UQOQ1" => "Svoboda Jaroslav", "U  62" => "Zapletal Miroslav", "USOR9" => "Babková Ivana", "UZOMX" => "Hejna Petr", "UVORV" => "Knotek Jan", "UVORT" => "Adámek Libor", "UQOPV" => "Čechlovská Dagmar", "UQOQ3" => "Chvojka Matěj", "UWOS3" => "Křivková Lucie", "USOR5" => "Nosek Zdeněk", "UQOQ0" => "Nosková Ivana", "UZOM5" => "Nová Jitka", "UOOOL" => "Stratílek Miloš", "UZOMO" => "Venzara Robert", "UQOQ2" => "Zeman Jaroslav", "UZOMQ" => "Přívratský Zdeněk", "UOOP2" => "Dus René", "U  65" => "Jech Ivo", "UXOSA" => "Kratochvíl Jan", "USOR3" => "Laubová Lenka", "U0108" => "Cach Zdeněk", "UOOPA" => "Neradová Soňa", "UPOPK" => "Panuška Ivan", "UROQM" => "Řehounek Luboš", "UROQO" => "Bartoš Petr", "UROQN" => "Svatoň Martin", "U  23" => "Jozífová Irena", "UTVCG" => "Fišar Petr", "UOONV" => "Hypšmanová Dobromila", "UROQE" => "Novotná Jitka", "UZOM9" => "Levá Gabriela", "U  33" => "Petera Martin", "UXOS9" => "Škrlant Jiří", "U  24" => "Kudrna Ivan"}; "parse teacher select")]
    #[test_case("selectedRoom", &map! {"15" => "EA31", "GZ" => "B302", "1M" => "baz2", "ZX" => "A103", "DT" => "B104", "14" => "D163", "08" => "E306", "05" => "B026", "0L" => "EB16", "FV" => "B333", "VF" => "B204", "09" => "E304", "1W" => "ED29", "0X" => "B101", "0V" => "EA25", "1T" => "SOK", "0R" => "EA28", "PL" => "B224", "0Q" => "EA20", "0T" => "EA29", "2F" => "TV", "16" => "EA30", "6B" => "B330", "LE" => "A101", "SR" => "B201", "B9" => "D055", "1Q" => "B010", "38" => "C214", "0S" => "EA22", "3S" => "C217", "EF" => "B130", "1R" => "EA4", "0Z" => "ED33", "0K" => "ELA4", "1X" => "EM-B", "0N" => "A003", "1O" => "PERG", "T0" => "B030", "1P" => "B007", "FO" => "B103", "51" => "B322", "G4" => "B203", "01" => "C113", "0M" => "C111", "5H" => "C203", "53" => "D251", "JI" => "B221", "0A" => "E322", "84" => "B321", "YC" => "EM-C", "1U" => "B009", "NP" => "C205", "IP" => "B401", "13" => "D263", "1L" => "D363", "04" => "C316", "35" => "D351", "0B" => "E309", "ZW" => "B303", "0W" => "EA21", "1N" => "D353", "0I" => "ELA2", "LG" => "B202", "9B" => "D153", "IZ" => "B206", "10" => "ED34", "3Z" => "D253", "OG" => "D352", "50" => "B323", "N5" => "B102", "1S" => "EA6", "U2" => "B301", "S8" => "B133", "06" => "D053", "C0" => "D252", "0J" => "ELA3", "RR" => "B306", "0O" => "A005", "0E" => "E319", "FR" => "EM-A", "1V" => "ED35", "95" => "B411", "ZZ" => "EM-D", "2B" => "A102", "77" => "B024", "0C" => "E318", "0D" => "TV2", "A5" => "B304", "9A" => "C317", "0Y" => "ED15", "07" => "B125", "30" => "B305", "YA" => "B332", "0G" => "ELA1", "32" => "C314", "ZY" => "baz", "00" => "B003", "0P" => "EA10", "G0" => "D152"}; "parse room select")]
    fn test_select(id: &str, result: &HashMap<String, String>) {
        let dom = html_parser::parse_html(DEFAULT_PAGE_SRC).unwrap();
        let dom = Dom::new(dom);

        assert_eq!(select(&dom, id).unwrap(), *result);
    }
}
