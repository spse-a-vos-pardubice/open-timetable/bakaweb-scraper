use crate::model::Hour;

use super::{vdom_wrapper::Dom, Error};

pub fn hours(dom: &Dom) -> Result<Vec<Hour>, Error> {
    // #main
    let main_el = dom
        .get_element_by_id("main")
        .ok_or(Error::MissingElement("#main".to_string()))?;
    let main_el_children = main_el.children();

    // #main > :first
    let hours_container_el = main_el_children
        .first()
        .ok_or(Error::MissingElement("#main > :first".to_string()))?;

    // #main > :first > *
    let hours_container_el_children = hours_container_el.children();
    hours_container_el_children
        .iter()
        .map(|hour_el| {
            let hour_el_children = hour_el.children();

            // #main > :first > * > :last
            let text_container = hour_el_children.last().ok_or(Error::MissingElement(
                "#main > :first > * > :last".to_string(),
            ))?;
            let text_container_children = text_container.children();

            // #main > :first > * > :last > :first
            let from = text_container_children
                .first()
                .ok_or(Error::MissingElement(
                    "#main > :first > * > :last > :first".to_string(),
                ))?
                .inner_text();
            let from = chrono::NaiveTime::parse_from_str(&from, "%H:%M")
                .map_err(|e| Error::InvalidContent(e.to_string()))?;

            // #main > :first > * > :last > :last
            let to = text_container_children
                .last()
                .ok_or(Error::MissingElement(
                    "#main > :first > * > :last > :last".to_string(),
                ))?
                .inner_text();
            let to = chrono::NaiveTime::parse_from_str(&to, "%H:%M")
                .map_err(|e| Error::InvalidContent(e.to_string()))?;

            let hour = Hour::new(from, to).map_err(|e| Error::InvalidContent(e.to_string()))?;
            Ok(hour)
        })
        .collect::<Result<Vec<Hour>, Error>>()
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod test {
    use super::*;

    use crate::html_parser;

    static DEFAULT_PAGE_SRC: &str = include_str!(concat!(
        env!("CARGO_MANIFEST_DIR"),
        "/test_html/2023-12-23/default_page.html"
    ));

    #[test]
    fn test_hours() {
        let expect = vec![
            Hour::new(
                chrono::NaiveTime::from_hms_opt(7, 5, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(7, 50, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(8, 00, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(8, 45, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(8, 50, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(9, 35, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(9, 45, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(10, 30, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(10, 50, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(11, 35, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(11, 45, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(12, 30, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(12, 40, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(13, 25, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(13, 35, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(14, 20, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(14, 25, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(15, 10, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(15, 15, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(16, 00, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(16, 5, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(16, 50, 0).unwrap(),
            )
            .unwrap(),
            Hour::new(
                chrono::NaiveTime::from_hms_opt(16, 55, 0).unwrap(),
                chrono::NaiveTime::from_hms_opt(17, 40, 0).unwrap(),
            )
            .unwrap(),
        ];

        let dom = html_parser::parse_html(DEFAULT_PAGE_SRC).unwrap();
        let dom = Dom::new(dom);

        assert_eq!(hours(&dom).unwrap(), expect);
    }
}
