use std::collections::HashSet;

use crate::model::{
    lesson,
    lesson::{FullClassGroup, Lesson, Teacher},
    Day, EntityType, Item, TermType,
};

use super::{
    vdom_wrapper::{Attribute, Dom, Element},
    Error,
};

pub fn days(dom: &Dom, term_type: &TermType, entity_type: &EntityType) -> Result<Vec<Day>, Error> {
    // #main
    let id_main = id_main(dom)?;

    // #main > .bk-timetable-row
    let cls_bk_timetable_row_vec = id_main.children_with_class("bk-timetable-row");

    cls_bk_timetable_row_vec
        .iter()
        .map(|cls_bk_timetable_row| parse_day(entity_type, cls_bk_timetable_row))
        .collect::<Result<Vec<Day>, Error>>()
}

fn id_main<'a>(dom: &'a Dom) -> Result<Element<'a>, Error> {
    dom.get_element_by_id("main")
        .ok_or(Error::MissingElement("#main".to_string()))
}

fn parse_day(entity_type: &EntityType, cls_bk_timetable_row: &Element) -> Result<Day, Error> {
    // #main > .bk-timetable-row > :last
    let cls_bk_cell_wrapper = cls_bk_timetable_row
        .last_child()
        .ok_or(Error::MissingElement(
            "#main > .bk-timetable-row > :last".to_string(),
        ))?;

    // TODO guards
    let mut items = Vec::new();

    // #main > .bk-timetable-row > :last > .bk-timetable-cell
    let cls_bk_timetable_cell_vec = cls_bk_cell_wrapper.children_with_class("bk-timetable-cell");
    if cls_bk_timetable_cell_vec.len() == 1 {
        if let Some(cls_day_item_volno) =
            cls_bk_timetable_cell_vec[0].first_child_with_class("day-item-volno")
        {
            let text = cls_day_item_volno.inner_text().trim().to_string();
            return Ok(Day::DayOff { text });
        }
    }

    for cls_bk_timetable_cell in cls_bk_timetable_cell_vec {
        // #main > .bk-timetable-row > :last > * > :first
        let optional_item = match cls_bk_timetable_cell.first_child() {
            Some(cls_day_item) => {
                // #main > .bk-timetable-row > :last > * > :first > *
                let lessons = cls_day_item
                    .children()
                    .iter()
                    .map(|cls_day_item_hover| parse_lesson(entity_type, cls_day_item_hover))
                    .collect::<Result<Vec<Lesson>, Error>>()?;

                Some(Item::new(lessons))
            }
            None => None,
        };
        items.push(optional_item);
    }

    Ok(Day::Normal { items })
}

fn parse_lesson(entity_type: &EntityType, cls_day_item_hover: &Element) -> Result<Lesson, Error> {
    let detail_text = extract_day_item_hover_detail_text(cls_day_item_hover)?;
    let detail_with_type: DetailWithType = parse_data_detail(&detail_text)?;

    match detail_with_type.type_.as_str() {
        "absent" | "removed" => Ok(Lesson::Removed),
        "atom" => parse_lesson_atom(entity_type, cls_day_item_hover, &detail_text),
        _ => Err(Error::InvalidContent(format!(
            "atom type {}",
            detail_with_type.type_
        ))),
    }
}

fn parse_lesson_atom(
    entity_type: &EntityType,
    cls_day_item_hover: &Element,
    detail_text: &str,
) -> Result<Lesson, Error> {
    let change = cls_day_item_hover.has_class("pink");
    let extract = extract_day_item_hover(cls_day_item_hover)?;

    let (subject_abbreviation, condition) = parse_subject_abbreviation(&extract.middle)?;

    let lesson_data = match entity_type {
        EntityType::Class => {
            let detail: DetailForClass = parse_data_detail(detail_text)?;

            lesson::LessonData::Class(lesson::for_class::Lesson {
                group: parse_class_group(&extract.top_left),
                classroom_name: extract.top_right,
                condition,
                subject: lesson::Subject {
                    name: parse_subjecttext(&detail.subjecttext),
                    abbreviation: subject_abbreviation,
                },
                teacher: lesson::Teacher {
                    name: detail.teacher,
                    abbreviation: extract.bottom,
                },
            })
        }
        EntityType::Teacher => {
            let detail: DetailForTeacher = parse_data_detail(detail_text)?;

            lesson::LessonData::Teacher(lesson::for_teacher::Lesson {
                groups: parse_full_class_groups(&detail.group)?,
                classroom_name: extract.top_right,
                condition,
                subject: lesson::Subject {
                    name: parse_subjecttext(&detail.subjecttext),
                    abbreviation: subject_abbreviation,
                },
            })
        }
        EntityType::Room => {
            let detail: DetailForRoom = parse_data_detail(detail_text)?;

            lesson::LessonData::Room(
                lesson::for_room::Lesson::new(
                    parse_full_class_groups(&detail.group)?,
                    condition,
                    lesson::Subject {
                        name: parse_subjecttext(&detail.subjecttext),
                        abbreviation: subject_abbreviation,
                    },
                    parse_teacher_for_room(&detail.teacher, &extract.bottom),
                )
                .map_err(|e| {
                    Error::InvalidContent(format!("could not construct room lesson: {e}"))
                })?,
            )
        }
    };

    Ok(if change {
        Lesson::Change(lesson_data)
    } else {
        Lesson::Normal(lesson_data)
    })
}

#[derive(Debug, serde::Deserialize)]
struct DetailWithType {
    #[serde(rename = "type")]
    pub type_: String,
}

#[derive(Debug, serde::Deserialize)]
struct DetailForClass {
    pub subjecttext: String,
    pub teacher: String,
}

#[derive(Debug, serde::Deserialize)]
struct DetailForTeacher {
    pub subjecttext: String,
    pub group: String,
}

#[derive(Debug, serde::Deserialize)]
struct DetailForRoom {
    pub subjecttext: String,
    pub teacher: String,
    pub group: String,
}

fn parse_data_detail<'a, T: serde::Deserialize<'a>>(detail_text: &'a str) -> Result<T, Error> {
    serde_json::from_str(detail_text).map_err(|e| {
        Error::InvalidContent(format!(
            "could not parse `data-detail` because of {e}: {detail_text}"
        ))
    })
}

fn parse_class_group(group_name: &str) -> lesson::ClassGroup {
    if group_name.is_empty() {
        lesson::ClassGroup::WholeClass
    } else {
        lesson::ClassGroup::GroupName(group_name.to_string())
    }
}

fn split_by_comma_and_trim(
    text: &str,
) -> std::iter::Map<std::str::Split<'_, char>, for<'a> fn(&'a str) -> &'a str> {
    text.split(',').map(str::trim)
}

fn parse_full_class_groups(detail_group: &str) -> Result<HashSet<FullClassGroup>, Error> {
    split_by_comma_and_trim(detail_group)
        .map(parse_full_class_group)
        .collect::<Result<HashSet<FullClassGroup>, Error>>()
}

fn parse_full_class_group(line: &str) -> Result<FullClassGroup, Error> {
    let [class_name, group_name] = line
        .split_whitespace()
        .collect::<Vec<&str>>()
        .try_into()
        .map_err(|_| {
            Error::InvalidContent(format!(
                "expected a single whitespace in full class/group name in text `{line}`"
            ))
        })?;

    Ok(if group_name == "celá" {
        FullClassGroup::ClassName(class_name.to_string())
    } else {
        FullClassGroup::ClassNameAndGroupName(class_name.to_string(), group_name.to_string())
    })
}

fn parse_subject_abbreviation(middle: &str) -> Result<(String, Option<lesson::Condition>), Error> {
    const CONDITION_PATTERN: &str = ": ";

    match middle.split_once(CONDITION_PATTERN) {
        Some((condition_str, subject)) => {
            use lesson::Condition::{EvenWeek, OddWeek};

            let condition = match condition_str {
                "L" => OddWeek,
                "S" => EvenWeek,
                _ => {
                    return Err(Error::InvalidContent(format!(
                        "unknown condition: `{condition_str}`"
                    )))
                }
            };

            Ok((subject.to_string(), Some(condition)))
        }
        None => Ok((middle.to_string(), None)),
    }
}

fn parse_subjecttext(detail_subjecttext: &str) -> String {
    // TODO: check if this is a valid use of unwrap without any side effects
    #[allow(clippy::unwrap_used)]
    detail_subjecttext
        .split('|')
        .next()
        .unwrap()
        .trim()
        .to_string()
}

fn parse_teacher_for_room(detail_teacher: &str, bottom: &str) -> Vec<Teacher> {
    split_by_comma_and_trim(detail_teacher)
        .zip(split_by_comma_and_trim(bottom))
        .map(|(name, abbreviation)| Teacher {
            name: name.to_string(),
            abbreviation: abbreviation.to_string(),
        })
        .collect::<Vec<Teacher>>()
}

struct ClsDayItemHoverContent {
    pub top_left: String,
    pub top_right: String,
    pub middle: String,
    pub bottom: String,
}

fn extract_day_item_hover_detail_text(cls_day_item_hover: &Element) -> Result<String, Error> {
    match cls_day_item_hover.attribute("data-detail") {
        Attribute::None => Err(Error::InvalidContent(
            "`cls_day_item_hover` is missing the `data-detail` attribute".to_string(),
        )),
        Attribute::WithoutValue => Err(Error::InvalidContent(
            "data-detail` attribute on a `cls_day_item_hover` is empty".to_string(),
        )),
        Attribute::Exists(detail) => Ok(detail),
    }
}

fn extract_day_item_hover(cls_day_item_hover: &Element) -> Result<ClsDayItemHoverContent, Error> {
    let cls_day_flex = cls_day_item_hover
        .first_child()
        .ok_or(Error::MissingElement(".day-flex".to_string()))?;
    let cls_top = cls_day_flex
        .first_child_with_class("top")
        .ok_or(Error::MissingElement(".day-flex .top".to_string()))?;

    let top_left = cls_top
        .first_child_with_class("left")
        .ok_or(Error::MissingElement(".day-flex .top .left".to_string()))?
        .inner_text()
        .trim()
        .to_string();
    let top_right = cls_top
        .first_child_with_class("right")
        .ok_or(Error::MissingElement(".day-flex .top .right".to_string()))?
        .inner_text()
        .trim()
        .to_string();

    let middle = cls_day_flex
        .first_child_with_class("middle")
        .ok_or(Error::MissingElement(".day-flex .middle".to_string()))?
        .inner_text()
        .trim()
        .to_string();

    let bottom = cls_day_flex
        .first_child_with_class("bottom")
        .ok_or(Error::MissingElement(".day-flex .bottom".to_string()))?
        .inner_text()
        .trim()
        .to_string();

    Ok(ClsDayItemHoverContent {
        top_left,
        top_right,
        middle,
        bottom,
    })
}
