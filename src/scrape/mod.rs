mod days;
mod hours;
mod select;
mod vdom_wrapper;

pub use days::days;
pub use hours::hours;
pub use select::select;
pub use vdom_wrapper::Dom;
pub use vdom_wrapper::Element;

use thiserror::Error;

#[derive(Debug, Error, serde::Serialize)]
pub enum Error {
    #[error("missing element: {0}")]
    MissingElement(String),
    #[error("invalid content: {0}")]
    InvalidContent(String),
}
