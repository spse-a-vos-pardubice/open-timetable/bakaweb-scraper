use super::{Day, Hour, Weekday};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("`items` count `{items}` does not match the `hours` count `{hours}` on {weekday:?}")]
    LessonsCountDoesNotMatchHoursCount {
        hours: usize,
        items: usize,
        weekday: Weekday,
    },
}

#[derive(Debug, derive_getters::Getters, serde::Serialize)]
pub struct Timetable {
    hours: Vec<Hour>,
    days: [Day; 5],
}

impl Timetable {
    pub fn new(hours: Vec<Hour>, days: [Day; 5]) -> Result<Self, Error> {
        for (day, weekday) in days.iter().zip([
            Weekday::Monday,
            Weekday::Tuesday,
            Weekday::Wednesday,
            Weekday::Thursday,
            Weekday::Friday,
        ]) {
            if let Day::Normal { items } = day {
                if hours.len() != items.len() {
                    return Err(Error::LessonsCountDoesNotMatchHoursCount {
                        hours: hours.len(),
                        items: items.len(),
                        weekday,
                    });
                }
            }
        }

        Ok(Timetable { hours, days })
    }
}
