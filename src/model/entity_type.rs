#[derive(Debug)]
pub enum EntityType {
    Class,
    Teacher,
    Room,
}
