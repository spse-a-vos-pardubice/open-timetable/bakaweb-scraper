use std::collections::HashMap;

#[derive(Debug, serde::Serialize)]
pub struct Metadata {
    pub class_map: HashMap<String, String>,
    pub teacher_map: HashMap<String, String>,
    pub room_map: HashMap<String, String>,
}
