#[derive(Debug)]
pub enum TermType {
    Permanent,
    Specific,
}
