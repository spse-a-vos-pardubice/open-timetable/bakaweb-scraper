use std::collections::HashSet;

use derive_getters::Getters;
use thiserror::Error;

use super::shared::{Condition, FullClassGroup, Subject, Teacher};

#[derive(Debug, Error)]
pub enum Error {
    #[error("`groups` count does not match `teachers` count")]
    GroupsCountDoesNotMatchTeacherCount,
}

#[derive(Debug, Getters, serde::Serialize)]
pub struct Lesson {
    groups: HashSet<FullClassGroup>,
    condition: Option<Condition>,
    subject: Subject,
    teachers: Vec<Teacher>,
}

impl Lesson {
    pub fn new(
        groups: HashSet<FullClassGroup>,
        condition: Option<Condition>,
        subject: Subject,
        teachers: Vec<Teacher>,
    ) -> Result<Self, Error> {
        if groups.len() != teachers.len() {
            return Err(Error::GroupsCountDoesNotMatchTeacherCount);
        }

        Ok(Lesson {
            groups,
            condition,
            subject,
            teachers,
        })
    }
}
