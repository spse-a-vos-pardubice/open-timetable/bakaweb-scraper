use super::shared::{ClassGroup, Condition, Subject, Teacher};

#[derive(Debug, serde::Serialize)]
pub struct Lesson {
    pub group: ClassGroup,
    pub classroom_name: String,
    pub condition: Option<Condition>,
    pub subject: Subject,
    pub teacher: Teacher,
}
