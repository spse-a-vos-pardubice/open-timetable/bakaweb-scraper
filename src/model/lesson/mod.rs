pub mod for_class;
pub mod for_room;
pub mod for_teacher;
pub mod shared;

pub use shared::ClassGroup;
pub use shared::Condition;
pub use shared::FullClassGroup;
pub use shared::Subject;
pub use shared::Teacher;

#[derive(Debug, serde::Serialize)]
#[serde(tag = "status")]
pub enum Lesson {
    Normal(LessonData),
    Change(LessonData),
    Removed,
}

#[derive(Debug, serde::Serialize)]
#[serde(tag = "type")]
pub enum LessonData {
    Class(for_class::Lesson),
    Teacher(for_teacher::Lesson),
    Room(for_room::Lesson),
}
