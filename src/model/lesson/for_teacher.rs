use std::collections::HashSet;

use super::shared::{Condition, FullClassGroup, Subject};

#[derive(Debug, serde::Serialize)]
pub struct Lesson {
    pub groups: HashSet<FullClassGroup>,
    pub classroom_name: String,
    pub condition: Option<Condition>,
    pub subject: Subject,
}
