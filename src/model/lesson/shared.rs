#[derive(Debug, serde::Serialize)]
#[serde(tag = "type")]
pub enum ClassGroup {
    WholeClass,
    GroupName(String),
}

#[derive(Debug, Hash, PartialEq, Eq, serde::Serialize)]
pub enum FullClassGroup {
    ClassName(String),
    ClassNameAndGroupName(String, String),
}

#[derive(Debug, serde::Serialize)]
pub enum Condition {
    OddWeek,
    EvenWeek,
}

#[derive(Debug, serde::Serialize)]
pub struct Subject {
    pub name: String,
    pub abbreviation: String,
}

#[derive(Debug, serde::Serialize)]
pub struct Teacher {
    pub name: String,
    pub abbreviation: String,
}
