#[derive(Debug)]
pub enum Term {
    Permanent,
    Actual,
    Next,
}
