use super::lesson::Lesson;

#[derive(Debug, serde::Serialize)]
pub struct Item {
    lessons: Vec<Lesson>,
}

impl Item {
    pub fn new(items: Vec<Lesson>) -> Self {
        Item { lessons: items }
    }
}
