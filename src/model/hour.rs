use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("time `{from}` is after `{to}`, `from` must be before or the same as `to`")]
    StartAfterEnd {
        from: chrono::NaiveTime,
        to: chrono::NaiveTime,
    },
}

#[derive(Debug, PartialEq, derive_getters::Getters, serde::Serialize)]
pub struct Hour {
    from: chrono::NaiveTime,
    to: chrono::NaiveTime,
}
impl Hour {
    pub fn new(from: chrono::NaiveTime, to: chrono::NaiveTime) -> Result<Self, Error> {
        if from > to {
            return Err(Error::StartAfterEnd { from, to });
        }
        Ok(Hour { from, to })
    }
}
