use super::item::Item;

#[derive(Debug, serde::Serialize)]
#[serde(tag = "type")]
pub enum Day {
    Normal { items: Vec<Option<Item>> },
    DayOff { text: String },
}
