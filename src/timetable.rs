use percent_encoding::{utf8_percent_encode, AsciiSet, CONTROLS};

use crate::{
    html_parser,
    model::{EntityType, Term, TermType, Timetable},
    scrape,
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    HtmlParser(html_parser::Error),
    #[error("could not scrape the website: {0}")]
    Scrape(scrape::Error),
    #[error("invalid id")]
    NotFound,
}

pub fn parse_and_scrape(
    term_type: &TermType,
    entity_type: &EntityType,
    id: &str,
    html: &str,
) -> Result<Timetable, Error> {
    let virtual_dom = html_parser::parse_html(html).map_err(Error::HtmlParser)?;
    let dom = scrape::Dom::new(virtual_dom);

    let select_id = get_select_id_by_entity_type(entity_type);
    let options = scrape::select(&dom, select_id).map_err(Error::Scrape)?;
    if !options.contains_key(id) {
        return Err(Error::NotFound);
    }

    let hours = scrape::hours(&dom).map_err(Error::Scrape)?;
    let days = scrape::days(&dom, term_type, entity_type)
        .map_err(Error::Scrape)?
        .try_into()
        .map_err(|_| {
            Error::Scrape(scrape::Error::MissingElement(
                "invalid amount of days in a week".to_string(),
            ))
        })?;

    Timetable::new(hours, days).map_err(|e| {
        Error::Scrape(scrape::Error::InvalidContent(format!(
            "could not construct timetable: {e}"
        )))
    })
}

#[must_use]
pub fn url(base_url: &str, term: &Term, entity_type: &EntityType, id: &str) -> String {
    format!(
        "{base_url}/{term}/{entity_type}/{escaped_id}",
        term = term_to_url_path_component(term),
        entity_type = entity_type_to_url_path_component(entity_type),
        escaped_id = url_encode(id)
    )
}

fn entity_type_to_url_path_component(entity_type: &EntityType) -> String {
    use EntityType::{Class, Room, Teacher};

    match entity_type {
        Class => "Class",
        Teacher => "Teacher",
        Room => "Room",
    }
    .to_string()
}

fn term_to_url_path_component(term: &Term) -> String {
    use Term::{Actual, Next, Permanent};

    match term {
        Permanent => "Permanent",
        Actual => "Actual",
        Next => "Next",
    }
    .to_string()
}

fn get_select_id_by_entity_type(entity_type: &EntityType) -> &'static str {
    match entity_type {
        EntityType::Class => "selectedClass",
        EntityType::Teacher => "selectedTeacher",
        EntityType::Room => "selectedRoom",
    }
}

fn url_encode(value: &str) -> String {
    const FRAGMENT: &AsciiSet = &CONTROLS.add(b' ').add(b'"').add(b'<').add(b'>').add(b'`');
    utf8_percent_encode(value, FRAGMENT).to_string()
}
