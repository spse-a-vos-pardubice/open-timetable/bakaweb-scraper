use crate::{html_parser, model::metadata::Metadata, scrape};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    HtmlParser(html_parser::Error),
    #[error("could not scrape the website: {0}")]
    Scrape(scrape::Error),
}

pub fn parse_and_scrape(html: &str) -> Result<Metadata, Error> {
    let virtual_dom = html_parser::parse_html(html).map_err(Error::HtmlParser)?;
    let dom = scrape::Dom::new(virtual_dom);

    Ok(Metadata {
        class_map: scrape::select(&dom, "selectedClass").map_err(Error::Scrape)?,
        teacher_map: scrape::select(&dom, "selectedTeacher").map_err(Error::Scrape)?,
        room_map: scrape::select(&dom, "selectedRoom").map_err(Error::Scrape)?,
    })
}

#[must_use]
pub fn url(base_url: &str) -> String {
    base_url.to_string()
}
